package com.example.restdocs.dto;

import com.example.restdocs.entity.Member;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class UpdateRequest {
    private String grade;
    private Integer year;

}
