package com.example.restdocs.controller;


import com.example.restdocs.dto.CreateRequest;
import com.example.restdocs.dto.Response;
import com.example.restdocs.dto.UpdateRequest;
import com.example.restdocs.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MemberController {

    private final MemberService memberService;
/*
*  1. member 조회
*  2. member 추가
*  3. member 수정
*  4. member 삭제
*  5. member 전체 조회
* */

    @GetMapping("/member/{name}")
    public Response getMember(@PathVariable String name){
        return memberService.getMember(name);
    }

    @PostMapping("/member")
    public void createMember(@RequestBody CreateRequest request){
        memberService.createMember(request);
    }

    @PutMapping("/member/{name}")
    public Response updateMember(@RequestBody UpdateRequest request, @PathVariable String name){
        return memberService.updateMember(request, name);
    }

    @DeleteMapping("/member/{name}")
    public Response deleteMember(@PathVariable String name){
        return memberService.deleteMember(name);
    }

    @GetMapping("/members")
    public List<Response> getAllMember(){
        return memberService.getAllMember();
    }

}
