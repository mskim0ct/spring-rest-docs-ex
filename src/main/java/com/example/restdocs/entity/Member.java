package com.example.restdocs.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@Table(name = "member")
public class Member {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Setter
    @Column
    private String name;
    @Setter
    @Column
    private Integer year;
    @Setter
    @Column
    private String grade;
}
